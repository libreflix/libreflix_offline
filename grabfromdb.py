# -*- coding: utf-8 -*-
# Author: Guilmour Rossi
# Description: This file grab films informations from Libreflix and export it to
# Markdown files to be generate with Libreflix Offline

import time
import sys
import os
reload(sys)
sys.setdefaultencoding("utf-8")
import urllib
import shutil

import requests
from pymongo import MongoClient
from bson.json_util import dumps

def main():
    print "Olá"
    print "Conectando..."
    client = MongoClient('mongodb://138.68.13.63:27017/')
    # client = MongoClient('mongodb://localhost:27019/')
    print "Conectado."

    # db = client.test
    db = client.libreflix
    print db
    col_watches = db.watches
    print col_watches
    # array = col_watches.find({"status":{"$exists": False}})
    # array = col_watches.find({"featured": "true"})
    array = col_watches.find({"canwecopy": "true"})

    print array
    filepath_img = "app/output/images/media/"
    if os.path.exists(filepath_img):
        shutil.rmtree(filepath_img)
    os.makedirs(filepath_img)


    print "Criando pastas dos filmes..."

    for w in array:
        print w["permalink"]

        path = "app/content/films/" + w["permalink"]
        if os.path.exists(path):
            shutil.rmtree(path)
        os.makedirs(path)

        filepath_md = "app/content/films/" + w["permalink"] + "/" + w["permalink"] + ".md"


        f = open(filepath_md,"w+")
        f.close()
        f = open(filepath_md, "a+")
        f.write("title: %s\r\n" % w["title"])
        f.write("date: 2010-12-03 10:20 \r\n")
        f.write("category: %s\r\n" % w["tags"])
        f.write("thumb480: %s\r\n" % ("/images/media/" + w["permalink"] + "_thumb.jpg"))

        img = open(filepath_img + w["permalink"] + "_thumb.jpg", "w+")
        img.write(requests.get("https://libreflix.org" + w["thumb480"] + "?resize=480,270&crop=entropy").content)
        img.close()

        f.write("imgbg: %s\r\n" % ("/images/media/" + w["permalink"] + "_bg.jpg"))

        img = open(filepath_img + w["permalink"] + "_bg.jpg", "w+")
        img.write(requests.get(w["imgbg"]).content)
        img.close()

        f.write("subtitle: %s\r\n" % w["subtitle"])
        f.write("year: %s\r\n" % w["year"])
        f.write("classind: %s\r\n" % w["classind"])
        f.write("duration: %s\r\n" % w["duration"])
        f.write("sinopse: %s\r\n" % w["sinopse"])



if __name__ == '__main__':
    main()
