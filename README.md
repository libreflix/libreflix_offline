# libreflix_offline


Versão offline do Libreflix que pode ser usado em redes comunitárias, redes locais ous rede com nenhum ou baixo acesso a internet comum.

## Instalação
Clonando o repositório

```sh
git clone https://libregit.org/libreflix/libreflix_offline.git
cd libreflix_offline
```

Construindo todo o catálogo:

```sh
chmod +x build.sh
./build.sh
```

Pronto, agora é só acessar http://localhost:3666
